package Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface WritableToFile {
    void writeToFile( String data) throws
            IOException;
}
