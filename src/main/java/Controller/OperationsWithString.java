package Controller;

import Model.Exceptions.IllegalParametersException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface OperationsWithString {
        String add(String first, String second) throws
                IllegalParametersException,
                IOException;
        String minus(String first, String second) throws
                IllegalParametersException,
                IOException;
        String multiply(String first,String second) throws
                IllegalParametersException,
                IOException;
        String divide(String first,String second) throws IllegalParametersException,
                IOException;
}
