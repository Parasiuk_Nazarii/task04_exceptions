package Controller;

import Model.Calc;
import Model.Exceptions.IllegalParametersException;
import Model.Exceptions.MyRuntimeException;

import java.io.*;


public class Controller implements WritableToFile, OperationsWithString {
    private Calc calc = new Calc();
    private final String filename = "log.txt";
    private FileWriter writer;
    private Double d1;
    private Double d2;

    public Controller() throws IOException {
    }


    public void writeToFile(String data) throws IOException {
        try {
            writer= new FileWriter(filename, true);
             writer.append("\n"+data);
        } catch (Exception e) {
            throw new MyRuntimeException("IO data problem to file ");
        }
        writer.close();
    }

    public String add(String first, String second) throws IllegalParametersException, IOException {
        String output;
        try {
            d1 = Double.parseDouble(first);
            d2 = Double.parseDouble(second);
        } catch (Exception e) {
            throw new IllegalParametersException("One or more parameters is not a number");
        }
        output = String.valueOf(calc.add(d1, d2));
        writeToFile(first + "+" + second + "=" + output);
        return output;
    }

    public String minus(String first, String second) throws IllegalParametersException, IOException {
        String output;
        try {
            d1 = Double.parseDouble(first);
            d2 = Double.parseDouble(second);
        } catch (Exception e) {
            throw new IllegalParametersException("One or more parameters is not a number");
        }
        output = String.valueOf(calc.minus(d1, d2));
        writeToFile(first + "-" + second + "=" + output);

        return output;
    }

    public String multiply(String first, String second) throws
            IllegalParametersException,
            IOException {
        String output;
        try {
            d1 = Double.parseDouble(first);
            d2 = Double.parseDouble(second);
        } catch (Exception e) {
            throw new IllegalParametersException("One or more parameters is not a number");
        }
        output = String.valueOf(calc.multiply(d1, d2));
        writeToFile(first + "*" + second + "=" + output);

        return output;
    }


    public String divide(String first, String second) throws IllegalParametersException, IOException {
        String output;
        try {
            d1 = Double.parseDouble(first);
            d2 = Double.parseDouble(second);
        } catch (Exception e) {
            throw new IllegalParametersException("One or more parameters is not a number");
        }
        if (d2 == 0) {
            throw new MyRuntimeException("Dividing by zero is not possible");
        }
        output = String.valueOf(calc.divide(d1, d2));
        writeToFile(first + "/" + second + "=" + output);

        return output;
    }

}
