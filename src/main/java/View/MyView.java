package View;

import Controller.Controller;
import Model.Exceptions.IllegalParametersException;
import Model.Exceptions.MyRuntimeException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    String first;
    String second;

    public MyView() throws IOException {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add");
        menu.put("2", "  2 - minus");
        menu.put("3", "  3 - multiply");
        menu.put("4", "  4 - divide");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }

    private void pressButton1()  {
        try {
            System.out.println(controller.add(first, second));
        } catch (IllegalParametersException | IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() throws IllegalParametersException, IOException {
        System.out.println(controller.minus(first,second));
    }
    private void pressButton3() throws IllegalParametersException,
            IOException {
        System.out.println(controller.multiply(first,second));
    }
    private void pressButton4() throws
            IllegalParametersException,
            MyRuntimeException, IOException {
        System.out.println(controller.divide(first,second));
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {

        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        System.out.println("\nStart:");
        System.out.println("Input first number");
        first = input.next();
        System.out.println("Input second number");
        second = input.next();
        System.out.println("What would you like to do with "+first +" and "+second);
        outputMenu();
        do {

            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}